//Declara objeto alumno

let alumno= [
    {
    "matricula":"2019030880",
    "nombre":"Quezada Ramos Julio Emiliano",
    "grupo" : "TI-73",
    "carrera" : "Tecnologias de la Informacion",
    "foto" : '<img src="/img/Quezada.jpg" alt="">'
    },
    {
        "matricula":"2021030143",
        "nombre":"Ibarra Hernandez Angel Antonio",
        "grupo" : "TI-73",
        "carrera" : "Tecnologias de la Informacion",
        "foto" : '<img src="/img/Ibarra Angel.png" alt="">'
    },
    {
        "matricula":"2021030314",
        "nombre":"Peñaloza Pizarro Felipe Andres",
        "grupo" : "TI-73",
        "carrera" : "Tecnologias de la Informacion",
        "foto" : '<img src="/img/Peñaloza Felipe.jpg" alt="">'
    },
    {
        "matricula":"2020030321",
        "nombre":"Ontiveros Govea Yair Alejandro",
        "grupo" : "TI-73",
        "carrera" : "Tecnologias de la Informacion",
        "foto" : '<img src="/img/Ontiveros Yair.jpg" alt="">'
    },
    {
        "matricula":"2021030311",
        "nombre":"Jonthan Alexis Reyes Lizarraga",
        "grupo" : "TI-73",
        "carrera" : "Tecnologias de la Informacion",
        "foto" : '<img src="/img/Reyes Jonathan.png" alt="">'
    },
    {
        "matricula":"2021030205",
        "nombre":"Tirado Rios Luis Mario",
        "grupo" : "TI-73",
        "carrera" : "Tecnologias de la Informacion",
        "foto" : ' <img src="/img/Tirado Mario.jpg" alt="">'      
    },
    {
        "matricula":"2021030118",
        "nombre":"Quezada Lara Jesus Alejandro",
        "grupo" : "TI-73",
        "carrera" : "Tecnologias de la Informacion",
        "foto" : '<img src="/img/Lara Jesus.jpg" alt="">'
    },
    {
        "matricula":"2020030714",
        "nombre":"Ruiz Guerrero Axel Jovani",
        "grupo" : "TI-73",
        "carrera" : "Tecnologias de la Informacion",
        "foto" : '<img src="/img/Ruiz Axel.jpg" alt="">'
    },
    {
        "matricula":"2021030077",
        "nombre":"Arias Tirado Mateo",
        "grupo" : "TI-73",
        "carrera" : "Tecnologias de la Informacion",
        "foto" : '<img src="/img/Arias Mateo.jpg" alt="">'
    },
    {
        "matricula":"2021030205",
        "nombre":"Pastrano Navarro Brandon Rogelio",
        "grupo" : "TI-73",
        "carrera" : "Tecnologias de la Informacion",
        "foto" : '<img src="/img/Pastrano Brandon.png" alt="">'
    }
    ];

    let tabla = document.getElementById('tabla')
    for(let i=0;i<alumno.length;i++){
        let fila = tabla.insertRow();


        let celdaMatricula = fila.insertCell(0);
        let celdaNombre = fila.insertCell(1);
        let celdaGrupo = fila.insertCell(2);
        let celdaCarrera = fila.insertCell(3);
        let celdaFoto = fila.insertCell(4);

        celdaMatricula.innerHTML= alumno[i].matricula;
        celdaNombre.innerHTML= alumno[i].nombre;
        celdaGrupo.innerHTML= alumno[i].grupo;
        celdaCarrera.innerHTML= alumno[i].carrera;
        celdaFoto.innerHTML= alumno[i].foto;


        if(i % 2 === 0){
            celdaMatricula.style.backgroundColor = '#0094ff';
            celdaNombre.style.backgroundColor = '#0094ff';
            celdaGrupo.style.backgroundColor = '#0094ff';
            celdaCarrera.style.backgroundColor = '#0094ff';
            celdaFoto.style.backgroundColor = '#0094ff';
        }
        else{
            celdaMatricula.style.backgroundColor = '#b2edff';
            celdaNombre.style.backgroundColor = '#b2edff';
            celdaGrupo.style.backgroundColor = '#b2edff';
            celdaCarrera.style.backgroundColor = '#b2edff';
            celdaFoto.style.backgroundColor = '#b2edff';
        }
    }


/*
console.log("Matricula: "+alumno.matricula);
console.log("Nombre: "+alumno.nombre);
console.log("Foto: "+alumno.foto);

alumno.nombre ="Acosta Diaz Maria"
console.log("Nombre: "+alumno.nombre);

//Objetos Compuestos

let cuentaBanco ={
    "numero":"10001",
    "banco":"Banorte",
        cliente:{
            "nombre":"Jose Lopez",
            "fechaNacimiento":"2020-01-01",
            "sexo":"Masculino",
        },
    "saldo":"10000"
}

console.log("Cliente:"+cuentaBanco.cliente.nombre);
console.log("Numero de Cuenta: "+cuentaBanco.numero);
console.log("Banco: "+cuentaBanco.banco);
console.log("Saldo: "+cuentaBanco.saldo);

//Declarar un arreglo de objetos

let Productos = [
    {
        "codigo":"1001",
        "descripcion":"Atún",
        "precio":"34"
    },
    {
        "codigo":"1002",
        "descripcion":"Jabon en Polvo",
        "precio":"23"
    },
    {
        "codigo":"1003",
        "descripcion":"Harina",
        "precio":"43"
    },
    {
        "codigo":"1004",
        "descripcion":"Pasta Dental",
        "precio":"78"
    }]

//Mostrar atributo de un elemento de un objeto del arreglo

console.log("La descripcion es "+ Productos[0].descripcion);
console.log("------------");
for(let i=0; i<Productos.length; i++){
    console.log("Codigo: "+Productos[i].codigo);
    console.log("Descripcion: "+Productos[i].descripcion);
    console.log("Precio: "+Productos[i].precio);
    console.log("------------");
}

*/