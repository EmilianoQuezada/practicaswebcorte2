/* Manejo de Arrays */

//Declaracion de array con elementos enteros




let arreglo=[4,89,30,10,34,89,10,5,8,28];

//Diseñar una funcion que recibe como argumento un arreglo 
// de enteros e imprime cada elemnto y el tamaño del arreglo

function mostrarArray(arreglo){
    let tamaño= arreglo.length;
    for( let con=0; con<tamaño; ++ con){
        console.log("Posicion "+con+": "+arreglo[con]);
    }
    console.log("Tamaño del arreglo: "+tamaño);
}
//Funcion para mostrar el promedio de los elementos del array.
function calcularPromedio(arreglo){
    let tamaño= arreglo.length;
    let suma=0;
    let promedio=0;

    for(let con=0;con<tamaño; ++con){
        suma = suma + (arreglo[con]);
    }
    promedio= suma / tamaño;
    console.log("Suma de los elementos del arreglo: "+suma);
    console.log("Promedio del arreglo: "+promedio.toFixed(2));
}

//Funcion para mostrar los valores pares de un arreglo.

function mostrarPar(arreglo){
    let tamaño= arreglo.length;
    for(let con=0;con<tamaño;++con){
        if((arreglo[con] % 2) == 0){
            console.log(arreglo[con]);
        }
    }
}

//Funcion para mostrar el valor mayor de los elemntos de un arreglo.
function mostrarMayor(arreglo){
    let tamaño = arreglo.length;
    let mayor=0;
    let posicion=0;

    for(let con=0;con<tamaño;++con){
        if(arreglo[con]>mayor){
            mayor=arreglo[con];
            posicion=con
        }
    }
    console.log("El elemento mayor es: "+mayor+" y se encuentra en la posicion: "+posicion);
}
//Funcion para llenar con valores aleatorios el arreglo.

function nuevoArreglo(arreglo){
    let tamaño=arreglo.length;
    console.log("Nuevo Arreglo:");
    for(let con=0;con<tamaño;con ++){
        arreglo[con]= (Math.random()*10).toFixed(0);
        console.log("Posicion "+con+": "+arreglo[con]);
    }
    console.log("Tamaño del arreglo: "+tamaño);
}

//Funcion que muestra el valor menor y la posicion del arreglo
function mostrarMenor(arreglo){
    let tamaño=arreglo.length;
    let menor=arreglo[0];
    let posicion=0;
    for(let con=0;con<tamaño;con++){
        if(arreglo[con]<menor){
            menor= arreglo[con];
            posicion=con;
        }
    }

    console.log("El elemento menor es: "+menor+" y se encuentra en la posicion:"+posicion)
}
//Funcion para comprobar si el generador de numeros aleatorios es simetrico es decir que la diferencia entre numeros
//pares e impares no sean mayor al 20%
function comprobarGenerador(arreglo){
    let tamaño= arreglo.length;
    let pares=0;
    let impares=0;
    let diferencia=0;
    let confiable = "";
    for(let con=0;con<tamaño;++con){
        if((arreglo[con] % 2) == 0){
            pares=pares+1;
        }
        else{
            impares=impares+1
        }
    }

    pares= (pares*100)/tamaño.toFixed(2);
    impares= (impares*100)/tamaño.toFixed(2);

    if(pares>impares){
        diferencia= pares-impares;
    }
    else if(impares > pares){
        diferencia= impares-pares;
    }
    else{
        diferencia=0;
    }

    if(diferencia>20){
        confiable= "No es simetrico";
    }
    else{
        confiable= "Es simetrico";
    }
    console.log("Porcentaje de pares: "+pares+"%");
    console.log("Porcentaje de impares: "+impares+"%");
    console.log("El generador "+ confiable);
}


console.log(mostrarArray(arreglo));
console.log(calcularPromedio(arreglo));
console.log(mostrarPar(arreglo));
console.log(mostrarMenor(arreglo));
console.log(mostrarMayor(arreglo));
console.log(nuevoArreglo(arreglo));
console.log(comprobarGenerador(arreglo));


//Variables y funciones necesarias para la pagina web



let comboBox=document.getElementById('combo');
let Paresp=document.getElementById('pares');
let Imparesp=document.getElementById('impares');
let simetriap=document.getElementById('simetrico');

function nuevoArregloPW(arreglo,tamaño){
    console.log("Arreglo 3");
    for(let con=0;con<tamaño;con ++){
        arreglo[con]= (Math.random()*50).toFixed(0);
        console.log(arreglo[con]+" posicion "+con);
    }
}

function mostrarArrayEnComboBox(arreglo,tamaño){
    if(tamaño>0){
        for(let con=0; con<tamaño; ++ con){
            let nuevoOption = document.createElement("option");
            nuevoOption.text = arreglo[con];
            comboBox.remove(nuevoOption);
        }
    }
    for( let con=0; con<tamaño; ++ con){
        let nuevoOption = document.createElement("option");
        nuevoOption.value=arreglo[con];
        nuevoOption.text= arreglo[con];
        comboBox.appendChild(nuevoOption);
    }
    
}

function comprobarSimetria(arreglo,tamaño){
    let porcientoPares=0;
    let cantidadPares=0;
    let porcientoImPares=0;
    let cantidadImPares=0;
    let diferencia=0;
    let simetria="";
    for(let con=0;con<tamaño;++con){
        if((arreglo[con] % 2) == 0){
            cantidadPares=cantidadPares+1;
        }
        else{
            cantidadImPares=cantidadImPares+1
        }
    }

    porcientoPares= ((cantidadPares*100)/tamaño).toFixed(0);
    porcientoImPares=((cantidadImPares*100)/tamaño).toFixed(0);

    Paresp.textContent=porcientoPares;
    Imparesp.textContent=porcientoImPares;

    if(porcientoPares>porcientoImPares){
        diferencia= porcientoPares-porcientoImPares
    }
    else if(porcientoImPares>porcientoPares){
        diferencia=porcientoImPares-porcientoPares
    }

    if(diferencia<=20){
        simetria="El generador es Simetrico"
    }
    else{
        simetria="El generador no es Simetrico"
    }

    simetriap.textContent=simetria;
}

function llenar(){
    let longuitud=document.getElementById('tamaño').value;
    let arreglo3=new Array(longuitud);
    nuevoArregloPW(arreglo3,longuitud);
    mostrarArrayEnComboBox(arreglo3,longuitud)
    comprobarSimetria(arreglo3,longuitud)
}