btnCalcular.addEventListener('click',function(){
    let Nombre= document.getElementById('nombre').value;
    let Edad=document.getElementById('edad').value
    let Estatura = document.getElementById('estatura').value;
    let Peso = document.getElementById('peso').value;
    let lblIMC =document.getElementById('IMC');
    let imagen=document.getElementById('resultado');
    let caloriasH=0;
    let caloriasM=0;
    let IMC= (Peso)/(Estatura*Estatura);
    let IMCFormato= IMC.toFixed(2)
    document.getElementById('iMC').value=IMCFormato;

    if(Edad>9 && Edad<18){
        caloriasH=(13.384*Peso)+658.6;
        caloriasM=(17.686*Peso)+692.2;
        console.log('VALOR H: ' + caloriasH);
        console.log('VALOR M: ' + caloriasM);
    }

    if(Edad>17 && Edad<31){
        caloriasH=(15.057*Peso)+692.2;
        caloriasM=(14.818*Peso)+486.6;
        console.log('VALOR H: ' + caloriasH);
        console.log('VALOR M: ' + caloriasM);
    }

    if(Edad>30 && Edad<61){
        caloriasH=(11.472*Peso)+873.1;
        caloriasM=(8.126*Peso)+845.6;
        console.log('VALOR H: ' + caloriasH);
        console.log('VALOR M: ' + caloriasM);
    }

    if(Edad>=60){
        caloriasH=(11.711*Peso)+587.7;
        caloriasM=(9.082*Peso)+685.5;
        console.log('VALOR H: ' + caloriasH);
        console.log('VALOR M: ' + caloriasM);
    }


    if(IMCFormato<18.5){
        document.getElementById('resultado').src = "../img/01.png";
    }
    if(IMCFormato>18.5 && IMCFormato<25){
        document.getElementById('resultado').src = "../img/02.png";
    }
    if(IMCFormato>=25 && IMCFormato<30){
        document.getElementById('resultado').src = "../img/03.png";
    }
    if(IMCFormato>=30 && IMCFormato<35){
        document.getElementById('resultado').src = "../img/04.png";
    }
    if(IMCFormato>=35 && IMCFormato<40){
        document.getElementById('resultado').src = "../img/05.png";
    }
    if(IMCFormato>=40){
        document.getElementById('resultado').src = "../img/06.png";
    }


    
    document.getElementById('caloriasH').value="Calorias por dia para Hombres"+caloriasH.toFixed(2);
    document.getElementById('caloriasM').value="Calorias por dia para Mujeres"+caloriasM.toFixed(2);
})